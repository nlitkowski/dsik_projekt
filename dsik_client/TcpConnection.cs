﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using System.IO;

namespace dsik_client
{
    class TcpConnection
    {
        const int BufferSize = 1024; 

        private List<TcpClientWrapper> tcpClients;

        public async Task SendMessage(string message, TcpClientWrapper currentTcp)
        {
            try
            {
                NetworkStream nwStream = currentTcp.GetStream();
                byte[] bytesToSend = Encoding.UTF8.GetBytes(message);
                await nwStream.WriteAsync(bytesToSend, 0, bytesToSend.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Błąd połączenia z serwerem", "Błąd");
            }
        }

        public async Task SendMessageBytes(byte[] message, TcpClientWrapper currentTcp)
        {
            try
            {
                NetworkStream nwStream = currentTcp.GetStream();
                await nwStream.WriteAsync(message, 0, message.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Błąd połączenia z serwerem", "Błąd");
            }
        }

        public async Task SaveFile(string file, long size, TcpClientWrapper currentTcp) 
        {
            try
            {
                int error = 0;
                var filename = Options.DownloadDir + '\\' + file;
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
                using (var fileStream = new FileStream(filename, FileMode.OpenOrCreate))
                {
                    NetworkStream nwStream = currentTcp.GetStream();
                    byte[] buffer = new byte[BufferSize];
                    int length = GetMessageBytes(buffer, currentTcp);
                    
                    if (Encoding.Default.GetString(buffer).Substring(0, "ERR".Length) == "ERR")
                    {
                        MessageBox.Show("Błąd przesyłania pliku");
                        error = 1;
                        return;
                    }
                    foreach(var byteToWrite in buffer)
                    {
                        fileStream.WriteByte(byteToWrite);
                    }
                    long fSize = length;
                    SendMessage("OK", currentTcp);
                    while (nwStream.DataAvailable || fSize < size)
                    {
                        buffer = new byte[BufferSize];
                        length = GetMessageBytes(buffer, currentTcp);
                        
                        if (Encoding.Default.GetString(buffer).Substring(0, "ERR".Length) == "ERR")
                        {
                            MessageBox.Show("Niepowodzenie w pobieraniu pliku", "Błąd");
                            return;
                        }
                        foreach (var byteToWrite in buffer)
                        {
                            fileStream.WriteByte(byteToWrite);
                        }
                        fSize += length;
                        SendMessage("OK", currentTcp);
                    }
                    SendMessage("ENDE", currentTcp);
                    MessageBox.Show("Pobrano plik!", "Sukces!");
                    nwStream.Flush();
                }
            }
            catch(IOException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Błąd wejścia pliku", "Błąd");
                SendMessage("ERR", currentTcp);
            }
            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Błąd połączenia z serwerem", "Błąd");
                SendMessage("ERR", currentTcp);
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Błąd połączenia z serwerem", "Błąd");
                SendMessage("ERR", currentTcp);
            }
        }

        public int GetMessageBytes(byte[] buffer, TcpClientWrapper currentTcp)
        {
            NetworkStream nwStream = currentTcp.GetStream();
            nwStream.ReadTimeout = 20000;
            int retVal = nwStream.Read(buffer, 0, BufferSize);
            return retVal;
        }

        public async Task<string> GetMessage(TcpClientWrapper currentTcp)
        {
            NetworkStream nwStream = currentTcp.GetStream();
            nwStream.ReadTimeout = 20000;
            byte[] bytesReceived = new byte[BufferSize];
            int received = await nwStream.ReadAsync(bytesReceived, 0, BufferSize);
            return Encoding.Default.GetString(bytesReceived);
        }

        public async Task SendFile(string filePath, string fileName, TcpClientWrapper currentTcp)
        {
            try
            {
                await SendMessage("FILESEND", currentTcp);
                var recvMsg = await GetMessage(currentTcp);
                if (recvMsg.Substring(0, "OK".Length) == "OK")
                {
                    await SendMessage(fileName, currentTcp);
                    recvMsg = await GetMessage(currentTcp);
                    if (recvMsg.Substring(0, "OK".Length) == "OK")
                    {
                        await SendMessage("OK" + new FileInfo(filePath).Length, currentTcp);
                        recvMsg = await GetMessage(currentTcp);
                        if (recvMsg.Substring(0, "OK".Length) == "OK")
                        {
                            //Read the contents of the file into a stream
                            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                            {
                                using (BinaryReader reader = new BinaryReader(fileStream))
                                {
                                    //Get current tcpClient network stream

                                    byte[] buffer = new byte[BufferSize];
                                    buffer = reader.ReadBytes(BufferSize);
                                    long fileSize = new FileInfo(filePath).Length;
                                    long lenSent = buffer.Length;
                                    await SendMessageBytes(buffer, currentTcp);
                                    while (lenSent < fileSize)
                                    {
                                        buffer = reader.ReadBytes(BufferSize);
                                        lenSent += buffer.Length;
                                        await SendMessageBytes(buffer, currentTcp);
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Niepowodzenie w przesyłaniu pliku", "Błąd");
                            currentTcp.GetStream().Flush();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Niepowodzenie w przesyłaniu pliku", "Błąd");
                        currentTcp.GetStream().Flush();
                    }
                }
                else
                {
                    MessageBox.Show("Niepowodzenie w przesyłaniu pliku", "Błąd");
                    currentTcp.GetStream().Flush();
                }

                recvMsg = await GetMessage(currentTcp);
                if (recvMsg.Substring(0, "OK".Length) == "OK")
                {
                    MessageBox.Show("Poprawnie wysłano plik", "Sukces!");
                    currentTcp.GetStream().Flush();
                }
                else
                {
                    MessageBox.Show("Niepowodzenie w przesyłaniu pliku", "Błąd");
                    currentTcp.GetStream().Flush();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Niepowodzenie w przesyłaniu pliku", "Błąd");
            }
        }

        public async Task DownloadFile(string filename, TcpClientWrapper currentTcp)
        {
            await SendMessage("FILE", currentTcp);
            var recvMsg = await GetMessage(currentTcp);
            if (recvMsg.Substring(0, "OK".Length) == "OK")
            {
                await SendMessage(filename, currentTcp);
                recvMsg = await GetMessage(currentTcp);
                if (recvMsg.Substring(0, "OK".Length) == "OK")
                {
                    await SendMessage("OK", currentTcp);
                    await SaveFile(filename, Int32.Parse(recvMsg.Substring("OK".Length).Trim('\0')), currentTcp);
                }
                else
                {
                    MessageBox.Show("Błąd pobierania", "Błąd");
                }
            }
        }

        public TcpConnection()
        {
            tcpClients = new List<TcpClientWrapper>();
        }

        public TcpClientWrapper AddTcpConnection(string host, int port)
        {
            try
            {
                var tcpClient = new TcpClientWrapper(host, port);
                if (tcpClient.Connected)
                {
                    MessageBox.Show("Połączono z serwerem!");
                    tcpClients.Add(tcpClient);
                    return tcpClient;
                }
                else
                {
                    MessageBox.Show("Nie udało się połączyć z serwerem.");
                    return null;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd łączenia z serwerem", "Błąd");
                Console.WriteLine(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Returns list of string representations of tcp client list
        /// </summary>
        public List<string> GetTcpClientsToString()
        {
            List<string> retList = new List<string>();
            foreach (var item in tcpClients)
            {
                retList.Add(item.ToString());
            }
            return retList;
        }

        /// <summary>
        /// Returns list of tcp clients
        /// </summary>
        /// <returns>TcpClientWrapper</returns>
        public List<TcpClientWrapper> GetTcpClients()
        {
            return this.tcpClients;
        }

        /// <summary>
        /// Closes given tcp client and returns tcp client list
        /// </summary>
        public List<TcpClientWrapper> RemoveTcpClient(int id)
        {
            tcpClients[id].Close();
            tcpClients.Remove(tcpClients[id]);
            return GetTcpClients();
        }

        /// <summary>
        /// Closes given tcp client and returns tcp client list
        /// </summary>
        public List<TcpClientWrapper> RemoveTcpClient(TcpClientWrapper tcpClient)
        {
            tcpClient.Close();
            tcpClients.Remove(tcpClient);
            return GetTcpClients();
        }
    }

    class TcpClientWrapper : TcpClient
    {
        public string Name { get; set; }
        public bool Busy { get; set; }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <exception cref="ArgumentNullException">Host or port are null</exception>
        /// <exception cref="SocketException">Socket is busy</exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public TcpClientWrapper(string host, int port) : base(host, port)
        {
            Name = host + ":" + port;
        }

        /// <summary>
        /// Returns string representation of TcpClient
        /// </summary>
        /// <returns>[host]:[port]</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
