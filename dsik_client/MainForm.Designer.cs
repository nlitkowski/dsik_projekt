﻿namespace dsik_client
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileSendDialog = new System.Windows.Forms.OpenFileDialog();
            this.fileSendBtn = new System.Windows.Forms.Button();
            this.connectBtn = new System.Windows.Forms.Button();
            this.disconnnectBtn = new System.Windows.Forms.Button();
            this.fileDownBtn = new System.Windows.Forms.Button();
            this.fileList = new System.Windows.Forms.ListBox();
            this.serverCmb = new System.Windows.Forms.ComboBox();
            this.downloadFolderDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileSendDialog
            // 
            this.fileSendDialog.FileName = "openFileDialog1";
            // 
            // fileSendBtn
            // 
            this.fileSendBtn.Location = new System.Drawing.Point(13, 220);
            this.fileSendBtn.Name = "fileSendBtn";
            this.fileSendBtn.Size = new System.Drawing.Size(75, 23);
            this.fileSendBtn.TabIndex = 0;
            this.fileSendBtn.Text = "Prześlij plik";
            this.fileSendBtn.UseVisualStyleBackColor = true;
            this.fileSendBtn.Click += new System.EventHandler(this.fileSendBtn_Click);
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(13, 13);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(59, 23);
            this.connectBtn.TabIndex = 1;
            this.connectBtn.Text = "Połącz";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // disconnnectBtn
            // 
            this.disconnnectBtn.Location = new System.Drawing.Point(78, 13);
            this.disconnnectBtn.Name = "disconnnectBtn";
            this.disconnnectBtn.Size = new System.Drawing.Size(75, 23);
            this.disconnnectBtn.TabIndex = 2;
            this.disconnnectBtn.Text = "Rozłącz";
            this.disconnnectBtn.UseVisualStyleBackColor = true;
            this.disconnnectBtn.Click += new System.EventHandler(this.disconnnectBtn_Click);
            // 
            // fileDownBtn
            // 
            this.fileDownBtn.Location = new System.Drawing.Point(112, 220);
            this.fileDownBtn.Name = "fileDownBtn";
            this.fileDownBtn.Size = new System.Drawing.Size(75, 23);
            this.fileDownBtn.TabIndex = 4;
            this.fileDownBtn.Text = "Pobierz";
            this.fileDownBtn.UseVisualStyleBackColor = true;
            this.fileDownBtn.Click += new System.EventHandler(this.fileDownBtn_Click);
            // 
            // fileList
            // 
            this.fileList.FormattingEnabled = true;
            this.fileList.Location = new System.Drawing.Point(13, 67);
            this.fileList.Name = "fileList";
            this.fileList.ScrollAlwaysVisible = true;
            this.fileList.Size = new System.Drawing.Size(174, 147);
            this.fileList.TabIndex = 5;
            this.fileList.DoubleClick += new System.EventHandler(this.fileList_DoubleClick);
            // 
            // serverCmb
            // 
            this.serverCmb.FormattingEnabled = true;
            this.serverCmb.Location = new System.Drawing.Point(13, 42);
            this.serverCmb.Name = "serverCmb";
            this.serverCmb.Size = new System.Drawing.Size(174, 21);
            this.serverCmb.TabIndex = 6;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(-1, 249);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(207, 23);
            this.progressBar.TabIndex = 7;
            // 
            // refreshBtn
            // 
            this.refreshBtn.Image = global::dsik_client.Properties.Resources.refresh1;
            this.refreshBtn.Location = new System.Drawing.Point(159, 13);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(28, 23);
            this.refreshBtn.TabIndex = 8;
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 272);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.serverCmb);
            this.Controls.Add(this.fileList);
            this.Controls.Add(this.fileDownBtn);
            this.Controls.Add(this.disconnnectBtn);
            this.Controls.Add(this.connectBtn);
            this.Controls.Add(this.fileSendBtn);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog fileSendDialog;
        private System.Windows.Forms.Button fileSendBtn;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.Button disconnnectBtn;
        private System.Windows.Forms.Button fileDownBtn;
        private System.Windows.Forms.ListBox fileList;
        private System.Windows.Forms.ComboBox serverCmb;
        private System.Windows.Forms.FolderBrowserDialog downloadFolderDlg;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button refreshBtn;
    }
}

