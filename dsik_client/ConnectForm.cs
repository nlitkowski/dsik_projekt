﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace dsik_client
{
    public partial class ConnectForm : Form
    {
        public string IpInput, IpAddress;
        public int Port;

        public ConnectForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IpInput = ipInputBox.Text;
                IPAddress ip = IPAddress.Parse(IpInput);
                IpAddress = ip.ToString();
                Port = int.Parse(portInputBox.Text);
            }
            catch
            {
                MessageBox.Show( "Wprowadzono złe dane", "Błąd");
                this.DialogResult = DialogResult.Retry;
            }
        }
    }
}
