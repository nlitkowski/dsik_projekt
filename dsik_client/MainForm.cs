﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;

namespace dsik_client
{
    public partial class MainForm : Form
    {
        
        bool DownDirSelected = false;
        TcpConnection tcpConnection;
        TcpClientWrapper currentTcp;
        BindingList<TcpClientWrapper> serverList;

        public MainForm()
        {
            InitializeComponent();

            refreshBtn.Image = (Image)new Bitmap(refreshBtn.Image, new Size(23, 23));

            buttonsSwitchEnabled(false);

            tcpConnection = new TcpConnection();

            fileList.Items.Add("Brak plików do wyświetlenia...");
            fileList.Enabled = false;

            fileSendDialog.InitialDirectory = Environment.SpecialFolder.DesktopDirectory.ToString();
            fileSendDialog.Filter = "Wszystkie pliki (*.*)|*.*|Pliki tekstowe (*.txt)|*.txt";
            fileSendDialog.RestoreDirectory = true;

            serverCmb.ValueMember = null;
            serverList = new BindingList<TcpClientWrapper>();
            RefreshBindingList();
            serverCmb.DataSource = serverList;
        }

        private void RefreshBindingList()
        {
            serverList.Clear();
            foreach(var item in tcpConnection.GetTcpClients())
            {
                serverList.Add(item);
            }
            serverCmb.Refresh();
        }

        private void buttonsSwitchEnabled(bool trueOrFalse)
        {
            fileDownBtn.Enabled = fileSendBtn.Enabled = disconnnectBtn.Enabled = refreshBtn.Enabled = trueOrFalse;
        }

        private async Task refreshFileList()
        {
            if(serverList.Count == 0)
            {
                fileList.Items.Clear();
                return;
            }
            fileList.Items.Clear();
            await tcpConnection.SendMessage("FILELIST", currentTcp);
            string JsonString = await tcpConnection.GetMessage(currentTcp);
            if (string.IsNullOrEmpty(JsonString) || JsonString.Substring(0, "EMPTY".Length) == "EMPTY") 
            {
                fileList.Items.Clear();
                fileList.Items.Add("Brak plików do wyświetlenia...");
                fileList.Enabled = false;
            }
            else
            {
                string[] files = JsonConvert.DeserializeObject<string[]>(JsonString);
                foreach(var item in files)
                {
                    fileList.Items.Add(item);
                }
                fileList.Enabled = true;
            }
        }

        private async void connectBtn_Click(object sender, EventArgs e)
        {
            ConnectForm popUp = new ConnectForm();
            var result = popUp.ShowDialog();
            if(result == DialogResult.OK)
            {
                try
                {
                    var tcpClient = tcpConnection.AddTcpConnection(popUp.IpAddress, popUp.Port);
                    if(tcpClient != null)
                    {
                        serverList.Add(tcpClient);
                        currentTcp = tcpClient;
                        currentTcp.Busy = false;
                        await refreshFileList();
                        
                        this.serverCmb.Refresh();
                        buttonsSwitchEnabled(true);
                    }
                }
                catch
                {
                    MessageBox.Show("Łączenie nie powiodło się", "Błąd");
                }
            }
            popUp.Dispose();
        }

        private async void disconnnectBtn_Click(object sender, EventArgs e)
        {
            await tcpConnection.SendMessage("QUIT", currentTcp);
            tcpConnection.RemoveTcpClient(currentTcp);
            RefreshBindingList();
            if (serverList.Count < 1)
            {
                buttonsSwitchEnabled(false);
            }
            else
            {
                serverCmb.SelectedItem = 0;
                currentTcp = (TcpClientWrapper)serverCmb.SelectedItem;
            }
            await refreshFileList();
        }

        private async void fileSendBtn_Click(object sender, EventArgs e)
        {
            if(!currentTcp.Busy)
            {
                currentTcp.Busy = true;
                var filePath = string.Empty;
                if (fileSendDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = fileSendDialog.FileName;
                    var fileName = fileSendDialog.SafeFileName;
                    await tcpConnection.SendFile(filePath, fileName, currentTcp);
                }
                currentTcp.Busy = false;
            }
            else
            {
                MessageBox.Show("Nie można wysłać pliku", "Połączenie zajęte");
            }
        }

        private async Task DownloadFile()
        {
            currentTcp.Busy = true;
            if (!DownDirSelected)
            {
                SelectDownloadDir();
            }
            else if (MessageBox.Show("Chcesz zmienić folder pobierania?",
            "Zmiana folderu pobierania",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SelectDownloadDir();
            }
            string filename = fileList.SelectedItem.ToString();
            await tcpConnection.DownloadFile(filename, currentTcp);
            currentTcp.Busy = false;
        }

        private async void fileDownBtn_Click(object sender, EventArgs e)
        {
            if (!currentTcp.Busy)
                await DownloadFile();
            else
                MessageBox.Show("Nie można pobrać pliku", "Połączenie zajęte");
        }

        private async void fileList_DoubleClick(object sender, EventArgs e)
        {
            if (!currentTcp.Busy)
                await DownloadFile();
            else
                MessageBox.Show("Nie można pobrać pliku", "Połączenie zajęte");
        }

        private void SelectDownloadDir()
        {
            if (downloadFolderDlg.ShowDialog() == DialogResult.OK)
            {
                Options.DownloadDir = downloadFolderDlg.SelectedPath;
                DownDirSelected = true;
            }
        }

        private async void refreshBtn_Click(object sender, EventArgs e)
        {
            if (!currentTcp.Busy)
                await refreshFileList();
            else
                MessageBox.Show("Nie można odświeżyć listy", "Połączenie zajęte");
        }
    }
}
