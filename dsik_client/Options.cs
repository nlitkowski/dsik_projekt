﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsik_client
{
    public class Options
    {
        public static string CurrentServer { get; set; }
        public static bool IsConnection { get; set; }
        public static string DownloadDir { get; set; }
    }
}
