from os import listdir
from os.path import isfile, join
import socket
import threading
import os
import json

mypath = '/home/students/s434720/Desktop/ProjektSIK/fileBase/'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
buffsize = 1024

print(onlyfiles)
print(len(json.dumps(onlyfiles)))


def get_files():
    return [f for f in listdir(mypath) if isfile(join(mypath, f))]


def file_list():
    json_string = json.dumps(get_files())
    length = len(json_string)
    if length > 0:
        return json_string
    else:
        return "EMPTY"


def connection(name, sock):
    while True:
        msg = sock.recv(buffsize)
        print(msg)
        if msg[:len('FILELIST')] == 'FILELIST':
            sock.send(file_list())
        elif msg[:len('FILESEND')] == 'FILESEND':
            upload(sock)
        elif msg[:len("FILE")] == 'FILE':
            sock.send("OK")
            msg = sock.recv(buffsize)
            print(msg)
            if isfile(mypath + msg):
                length = os.path.getsize(mypath+msg)
                sock.send("OK" + str(length))
                path = msg
                msg = sock.recv(buffsize)
                print(msg)
                if msg[:len("OK")] == "OK":
                    send_file(path, sock, length)
                else:
                    sock.send("ERR")
            else:
                sock.send('ERR')
        elif msg[:len('QUIT')] == 'QUIT':
            sock.close()
            break
        else:
            print("ERROR")


def upload(sock):
    sock.send('OK')
    msg = sock.recv(buffsize)
    print(msg)
    if msg[:len('ERR')] != 'ERR':
        filename = msg
        sock.send('OK')
        msg = sock.recv(buffsize)
        print(msg)
        if msg[:len("OK")] == "OK":
            file_len = long(msg[len("OK"):])
            sock.send('OK')
            with open(mypath + filename, 'wb') as f:
                data = sock.recv(buffsize)
                total_recv = len(data)
                print "{0:.2f}".format((total_recv / float(file_len)) * 100) + "% Done"
                f.write(data)
                print(data)
                while total_recv < file_len:
                    data = sock.recv(buffsize)
                    total_recv += len(data)
                    f.write(data)
                    print "{0:.2f}".format((total_recv/float(file_len)) * 100) + "% Done"
            sock.send('OK')
            f.close()
            print "Download Complete!"


def send_file(filename, sock, length):
    try:
        with open(mypath + filename, 'rb') as f:
            bytes_to_send = f.read(buffsize)
            sock.send(bytes_to_send)
            len_sent = len(bytes_to_send)
            msg = sock.recv(buffsize)
            print "{0:.2f}".format((len_sent / float(length)) * 100) + "% Done"
            if msg.strip() == "OK":
                while bytes_to_send != "":
                    bytes_to_send = f.read(buffsize)
                    sock.send(bytes_to_send)
                    len_sent += len(bytes_to_send)
                    print "{0:.2f}".format((len_sent / float(length)) * 100) + "% Done"
                    msg = sock.recv(buffsize)
                    if "ENDE" in msg:
                        print("Send complete!")
                        break
                    elif msg[:len("OK")] != "OK":
                        sock.send("ERR")
                        break
    except IOError as e:
        sock.send("ERR")
        print(e)
        print("IOError ;((")
    except socket.error as e:
        sock.send("ERR")
        print(e)
        print("Socket Error :((")


def main():
    host = '0.0.0.0'
    port = 4200

    s = socket.socket()
    s.bind((host, port))

    s.listen(5)

    temp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    temp_sock.connect(('8.8.8.8', 80))
    host_ip = temp_sock.getsockname()[0]
    temp_sock.close()

    print "Server Started. Listening on {}:{}".format(host_ip, port)
    while True:
        c, addr = s.accept()
        print "Client connected. IP: " + str(addr)
        t = threading.Thread(target=connection, args=("ConnThread", c))
        t.start()


if __name__ == '__main__':
    main()
