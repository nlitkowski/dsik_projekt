import socket

def Main():
    host = '127.0.0.1'
    port = 5000

    s = socket.socket()
    s.connect((host,port))

    message = raw_input("Message? -> ")
    if message != 'q':
        data = s.recv(1024)
        if data[:6] == 'EXISTS':
            msg = raw_input("Files Exist. Wanna continue? (Y/N)")
            if msg == 'Y':
                s.send('OK')
                files = s.recv(1024)
                print(files)
                filename = raw_input("Filename? -> ")
                if filename != 'q':
                    s.send(filename)
                    data = s.recv(1024)
                    if data[:6] == 'EXISTS':
                        filesize = long(data[6:])
                        message = raw_input("File Exists, " + str(filesize) + " Bytes, download? (Y/N)? -> ")
                        if message == 'Y':
                            s.send('OK')
                            f = open('new_'+filename, 'wb')
                            data = s.recv(1024)
                            totalRecv = len(data)
                            f.write(data)
                            while totalRecv < filesize:
                                data = s.recv(1024)
                                totalRecv += len(data)
                                f.write(data)
                                print "{0:.2f}".format((totalRecv/float(filesize))*100)+"% Done"
                            print "Download Complete!"
                    else:
                        print "File does not exist!"
        else:
            print "No files available"
    s.close()

if __name__ == '__main__':
    Main()
